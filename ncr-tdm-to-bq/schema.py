import json

with open('swagger.json') as f:
    swagger = f.read()
swagger = json.loads(swagger)['components']['schemas']

# swagger to BQ
dtype_map = {
    'boolean': 'BOOLEAN',
    'number': 'NUMERIC',
    'string': 'STRING',
    'integer': 'INT64'
}


def recursive_schema_parse(fld_name, schema):
    out = {'name': fld_name}

    if schema.get('$ref'):
        if 'CanonicalTransaction' in schema.get('$ref'):
            out = recursive_schema_parse(fld_name, swagger['Transaction'])
        else:
            sch_name = schema['$ref'].replace('#/components/schemas/', '')
            out = recursive_schema_parse(fld_name, swagger[sch_name])
    elif fld_name == 'customProperties':
        out['type'] = 'STRING'
    elif schema.get('type') and dtype_map.get(schema['type']):
        if fld_name == 'dateTime':
            out['type'] = 'TIMESTAMP'
        else:
            out['type'] = dtype_map[schema['type']]
    elif schema.get('type') == 'object':
        fields = []
        for nm, schm in schema['properties'].items():
            fields.append(recursive_schema_parse(nm, schm))
        out['type'] = 'STRUCT'
        out['fields'] = fields
    elif schema.get('type') == 'array':
        out['type'] = 'RECORD'
        out['mode'] = 'REPEATED'

        # take just fields property when repeated object is a struct
        array_item = recursive_schema_parse(fld_name, schema['items'])
        if array_item.get('type') == 'STRUCT':
            array_item = array_item['fields']

        out['fields'] = array_item
    else:
        raise Exception('type not recognized')
    return out


top_level = recursive_schema_parse(
    'CanonicalTLogViewData',
    swagger['CanonicalTLogViewData']
)['fields']
with open('out.json', 'w') as f:
    f.write(json.dumps(top_level))

# bq mk --table proj-id:dataset_name.table_name out.json
