terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.18.0"
    }
  }
  backend "gcs" {
    bucket = "andrew-romero-training"
    prefix = "terraform/state"
  }
}

provider "google" {
  #   credentials = file("<NAME>.json")
  project = "andrew-romero-training"
  region  = "us-central1"
}


resource "google_pubsub_topic" "df-messages-topic" {
  name = "df-messages"

  message_retention_duration = "86600s"
}
