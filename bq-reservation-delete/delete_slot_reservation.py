# https://googleapis.dev/python/bigqueryreservation/latest/index.html

from datetime import datetime
import time
from google.cloud import bigquery
from google.cloud.bigquery_reservation_v1.services import reservation_service



def delete_reservation(request=None):
    location = "US"  # reservation loc
    project_id = "my-project"  # project id
    res_id = "my-reservation"  # reservation to cancel

    res_parent = f'projects/{project_id}/locations/{location}'
    res_path = f'projects/{project_id}/locations/{location}/reservations/{res_id}'
    
    res_client = reservation_service.ReservationServiceClient()
    all_res = [r.name for r in res_client.list_reservations(parent=res_parent)]


    if res_path in all_res:
        # delete any assignments
        print(f'Found reservation {res_path}')
        asmts = res_client.list_assignments(parent=res_path)
        for asmt in asmts:
            print('Deleting all assignments for reservation.')
            res_client.delete_assignment(name=asmt.name)
        delete_time = datetime.utcnow()
        print(f'All assignments removed at {delete_time}.')

        # wait for jobs submitted before deletion time to complete
        while True:
            bqc = bigquery.Client()
            await_jobs = bqc.list_jobs(max_creation_time=delete_time, state_filter="running")
            await_jobs = [j.job_id for j in await_jobs]
            if len(await_jobs):
                print(f"Waiting for job IDs {await_jobs}")
                time.sleep(10)
            else:
                print('No jobs waiting.')
                break
        
        res_client.delete_reservation(name=res_path)
        print(f"Reservation {res_path} deleted")

    else:
        print(f'No reservation {res_path} found')

    return "Great success"


if __name__ == "__main__":
    delete_reservation()
